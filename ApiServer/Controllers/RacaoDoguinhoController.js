const express = require('express');
const racaoService = require('../Db/Models/racao');

const router = express.Router();

router.post('/', async (req, res) => {
    racaoService.create(req.body);
    res.status(201).send(req.body);
});

router.get('/', async (req, res) => {
    let compras = await racaoService.find();
    res.send(compras);
});

router.delete('/', async (req, res) => {
    await racaoService.remove();
    res.send('All deleted');
});

module.exports = app => app.use('/doguinho', router);
