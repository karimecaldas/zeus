const express = require('express');

const route = express.Router();

route.get('/world', async (req, res) => {
    res.send('Hello World');
});

module.exports = app => app.use('/hello', route);