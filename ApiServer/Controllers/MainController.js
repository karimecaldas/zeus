const express = require('express');
const bodyPaser = require('body-parser');

const app = express();

app.use(async function(req, res, next) {
    await res.header("Access-Control-Allow-Origin", "*");
    await res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    await res.header("Access-Control-Allow-Methods", "*");
    next();
});

app.use(bodyPaser.json());
app.use(bodyPaser.urlencoded({extended: false}));


require('./RacaoDoguinhoController')(app);

app.listen(3000);

app.get('/', (req, res) => {
    res.send('Uma requisição sem classe separada para o controller, também funciona');
});