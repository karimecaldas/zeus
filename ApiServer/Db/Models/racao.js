const mongoose = require('../dataBase');

const racaoSchema = new mongoose.Schema({
    compraId: {
        type: mongoose.Types.ObjectId
    },
    qtd: {
        type: Number,
        required: true
    },
    valor: {
        type: Number,
        require: true
    }
});

const Racao = mongoose.model('Racoe', racaoSchema);
module.exports = Racao;