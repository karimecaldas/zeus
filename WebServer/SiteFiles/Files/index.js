let totalQtd = 0;
let totalPreco = 0;

let totalPrecoHtml = document.getElementById('totalPreco');
let totalQtdHtml = document.getElementById('totalQtd');

addRacao = () => {
    let qtdInp = document.getElementById('qtdRecao');
    let precoInp = document.getElementById('preco');

    let xmlReq = new XMLHttpRequest();
    xmlReq.open('POST', 'http://localhost:3000/doguinho', true);
    xmlReq.setRequestHeader('Content-Type', 'application/json');

    xmlReq.onreadystatechange = () => {
        if (xmlReq.readyState === XMLHttpRequest.DONE &&  xmlReq.status === 201) {
            console.log('Create Response', xmlReq.responseText.toString());
            getRegisters();
        }
    }

    let obj = {
        qtd: Number(qtdInp.value),
        valor: Number(precoInp.value)
    }

    xmlReq.send (JSON.stringify(obj)); 
}

getRegisters = () => {
    let xmlReq = new XMLHttpRequest();
    xmlReq.open('GET', 'http://localhost:3000/doguinho', true);

    let parent = document.querySelector('.table');
    parent.innerHTML = '';
    xmlReq.onload = () => {
        console.log('Response Get', xmlReq.responseText);
        let all = JSON.parse(xmlReq.responseText);
        totalPreco = 0;
        totalQtd = 0;


        all.forEach(e => {
            getNewRegistryValue(parent, e);
        });
        totalPrecoHtml.innerHTML = totalPreco;
        totalQtdHtml.innerHTML = totalQtd;
    }
    xmlReq.send(null);
}

getNewRegistryValue = (parent, racao) => {
    let content = document.createElement('div');
    content.classList.add('table-contet');

    let divKg = document.createElement('div');
    let spanKg = document.createElement('span');
    spanKg.innerHTML = racao.qtd;

    let imgKg = document.createElement('img');
    imgKg.src = './SiteFiles/Files/Imagens/kg.png';

    divKg.appendChild(spanKg);
    divKg.appendChild(imgKg);
    
    let divDol = document.createElement('div');

    let spanDol = document.createElement('span');
    spanDol.innerHTML = racao.valor;
    
    let imgDol = document.createElement('img');
    imgDol.src = './SiteFiles/Files/Imagens/cash.ico';
    
    divDol.appendChild(spanDol);
    divDol.appendChild(imgDol);
    
    content.appendChild(divKg);
    content.appendChild(divDol);

    parent.appendChild(content);
    totalQtd += racao.qtd;
    totalPreco += racao.valor;
}

deleteRacoes = () => {
    let xmlReq = new XMLHttpRequest();
    xmlReq.open('DELETE', 'http://localhost:3000/doguinho', false);
    xmlReq.setRequestHeader('Content-Type', 'application/json');
    xmlReq.onload = () => {};
    xmlReq.send(null);    
    getRegisters();
}

getRegisters();